"use strict";

// [Section] Exponent Operator
    // Before ES6
    const firstNum = 8 ** 2;
    console.log(firstNum);

    // After ES6
    const secondNum = Math.pow(8, 2);
    console.log(secondNum);

// [Section] Template Literals
    /* 
        - allows to write string without using concatenation operator(+)
        - greatly helps with code readability
    */

    let myName = "John";
    // Before ES6
    console.log("Hello " + myName + ".");

    // After ES6
    // Using backticks (``)
    console.log(`Hello ${myName}.
    New Line`);

    // Template literals allows us to write strings with embedded JavaScript
    const interestRate = 0.1;
    const principal = 1000;
    console.log(`The interest on your savings account is: ${interestRate * principal}`);

// [Section] Array Destructuring
    /* 
        - allows us to unpack elements in arrays into distinct variables
        - allows us to name array elements with variables instead of using index numbers
        - it will help us with code readability
    */

    const fullName = ["Juan", "Dela", "Cruz"];
    // Before array destructuring
    console.log(fullName[0]);
    console.log(fullName[1]);
    console.log(fullName[2]);
    console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}`)

    // Array destructuring
    const [firstName, middleName, lastName] = fullName;
    console.log(firstName);
    console.log(middleName);
    console.log(lastName);
    console.log(`Hello ${firstName} ${middleName} ${lastName}`)

    console.log(fullName);

// [Section] Object Destructuring
    /* 
        - it allows us to unpack properties of objects into distinct variables
    */

    const person = {
        givenName: "Jane",
        maidenName: "Dela",
        familyName: "Cruz"
    }

    // Before
    console.log(person.givenName);      //dot notation
    console.log(person.maidenName);     //bracket notation
    console.log(person.familyName);

    // After
    const {familyName, givenName, maidenName} = person;
    console.log(givenName);  
    console.log(maidenName); 
    console.log(familyName);

    function getFullName({givenName, maidenName, familyName}) {
        console.log(`${givenName} ${maidenName} ${familyName}`);
    }

    getFullName(person);

// [Section] Arrow Functions
    /* 
        - it compacts alternative syntax to a traditional functions.
        - it is useful for code snippets where creating functions will not be used in any other portion of the code.
    */

    const hello1 = function() {
        console.log("Hello World");
    }
    hello1();

    const hello2 = () => {
        console.log("Hello World");
    }
    hello2();

    // Before arrow function and template literals
        function printFullName(firstName, middleInitial, lastName) {
            console.log(firstName + " " + middleInitial + " " + lastName);
        }

        printFullName("Chris", "O.", "Mortel");

    // After
        let fullname = (firstName, middleInitial, lastName) => {
            console.log(`${firstName} ${middleInitial} ${lastName}`);
        }

        fullname("Chris", "O.", "Mortel");

    // Arrow functions with loops
    const student = ["John", "Jane", "Judy"];

        // Before
        // for (let i=0; i<student.length; i++) {
        //     console.log(student[i]);
        // }

        student.forEach(function(student){
            console.log(student + " is a student");
        })

        // After
        student.forEach((student) => {
            console.log(`${student} is a student`);
        })
        
// [Section] Implicit Return statement
    /* 
        - there are instances when you can omit return statement
        - this works because even without return statement
        - Javascript implicitly adds it for the result of the function
    */

    const add = (x, y) => {
        console.log(x+y);
        return x+y;
    }

    let sum = add(23, 45);
    console.log(sum);

    const subtract = (x, y) => x-y;
    let difference = subtract(10, 5);
    console.log(difference);

// [Section] Default Function Argument Value
    /* 
        - provide a default argument value if none is provided when the function is invoked.
    */

    const greet = (name="User") => {
        return `"Good Morning, ${name}!"`;
    }

    console.log(greet());

// [Section] Class-based Object Blueprints
    /* 
        - allows us to create/instantiation of objects using classes blueprints
        - creating class - constructor is a special method of a class for creating/initializing an object for that class
    */
   
    class Car {
        constructor(brand, name, year) {
            this.carBrand = brand;
            this.carName = name;
            this.carYear = year;
        }   
    }

    let car = new Car("Toyota", "Hi-Lux", "2015");
    console.log(car);

    // Object.freeze(car);
    car.carBrand = "Tesla";

    // car.carPrice = "1";
    console.log(car);

    // delete car.carBrand;